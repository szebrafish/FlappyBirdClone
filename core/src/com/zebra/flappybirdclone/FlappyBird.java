package com.zebra.flappybirdclone;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;

import java.util.Random;

public class FlappyBird extends ApplicationAdapter {

	SpriteBatch batch;
    ShapeRenderer shapeRenderer;

	Texture background, gameover;
    Texture tubeTop1, tubeBottom1, tubeTop2, tubeBottom2, tubeTop3, tubeBottom3, tubeTop4, tubeBottom4;

    Animation<TextureRegion> animation;
    TextureAtlas atlas;

    Circle birdCircle;
    Rectangle tubeTopRect1, tubeBottomRect1, tubeTopRect2, tubeBottomRect2, tubeTopRect3, tubeBottomRect3, tubeTopRect4, tubeBottomRect4;

    BitmapFont font;

    Random random;

    float stateTime = 0f;

    float birdY = 0f;
    float birdVelocity = 0f;

    float tubeX1 = 0f;
    float tubeX2 = 0f;
    float tubeX3 = 0f;
    float tubeX4 = 0f;
    float tubeYOffset1 = 0f;
    float tubeYOffset2 = 0f;
    float tubeYOffset3 = 0f;
    float tubeYOffset4 = 0f;

    float gravity = 1.1f;
    float tubeVelocity = 10f;
    float tubeGap = 200f;
    float tubeDistance = 1050f;

    boolean flapping = false;
    boolean readyToScore = true;
    boolean gameOver = false;

    int score = 0;

	
	@Override
	public void create () {

		batch = new SpriteBatch();
        shapeRenderer = new ShapeRenderer();

        birdCircle = new Circle();
        tubeTopRect1 = new Rectangle();
        tubeBottomRect1 = new Rectangle();
        tubeTopRect2 = new Rectangle();
        tubeBottomRect2 = new Rectangle();
        tubeTopRect3 = new Rectangle();
        tubeBottomRect3 = new Rectangle();
        tubeTopRect4 = new Rectangle();
        tubeBottomRect4 = new Rectangle();

        background = new Texture("bg.png");
        gameover = new Texture("gameover.png");
        tubeTop1 = new Texture("toptube.png");
        tubeBottom1 = new Texture("bottomtube.png");
        tubeTop2 = new Texture("toptube.png");
        tubeBottom2 = new Texture("bottomtube.png");
        tubeTop3 = new Texture("toptube.png");
        tubeBottom3 = new Texture("bottomtube.png");
        tubeTop4 = new Texture("toptube.png");
        tubeBottom4 = new Texture("bottomtube.png");

        //Creates bird animation from 2 frames, sets animation speed and loop setting
        atlas = new TextureAtlas("birds.pack");
        animation = new Animation<TextureRegion>(0.25f, atlas.findRegion("bird"), atlas.findRegion("bird2"));
        animation.setPlayMode(Animation.PlayMode.LOOP);

        //Sets font settings for score display (plus texture filtering for smoother scaling)
        font = new BitmapFont();
        font.setColor(Color.GOLD);
        font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        font.getData().setScale(10);

        random = new Random();

        //Sets initial bird location
        birdY = (Gdx.graphics.getHeight() / 2) - (animation.getKeyFrame(stateTime).getRegionHeight() / 2);

        //Sets initial tube locations and distances
        tubeX1 = Gdx.graphics.getWidth() + tubeDistance;
        tubeX2 = tubeX1 + tubeDistance;
        tubeX3 = tubeX2 + tubeDistance;
        tubeX4 = tubeX3 + tubeDistance;

        //Sets initial tube up/down offsets
        tubeYOffset1 = 0f;
        tubeYOffset2 = (random.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - tubeGap - 500);
        tubeYOffset3 = (random.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - tubeGap - 500);
        tubeYOffset4 = (random.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - tubeGap - 500);

        score = 0;
        stateTime = 0f;
        birdVelocity = 0f;
        readyToScore = true;
        gameOver = false;
    }

	@Override
	public void render () {

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        //Draws background
        batch.draw(background, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        if (gameOver == false) {

            //Bird starts falling once screen is first clicked
            if (flapping) {

                //Bumps bird up when screen clicked
                if (Gdx.input.justTouched()) {

                    birdVelocity = -22.5f;
                }

                birdVelocity = birdVelocity + gravity;
                birdY -= birdVelocity;

                //Watches for initial click and begins moving bird ^^
            } else {

                if (Gdx.input.justTouched()) {

                    flapping = true;

                }
            }

            // Moves tubes along screen
            tubeX1 = tubeX1 - tubeVelocity;
            tubeX2 = tubeX2 - tubeVelocity;
            tubeX3 = tubeX3 - tubeVelocity;
            tubeX4 = tubeX4 - tubeVelocity;

        }

        //Draws tubes
        batch.draw(tubeTop1, tubeX1, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset1);
        batch.draw(tubeBottom1, tubeX1, (Gdx.graphics.getHeight() / 2) - tubeBottom1.getHeight() - tubeGap + tubeYOffset1);
        batch.draw(tubeTop2, tubeX2, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset2);
        batch.draw(tubeBottom2, tubeX2, (Gdx.graphics.getHeight() / 2) - tubeBottom2.getHeight() - tubeGap + tubeYOffset2);
        batch.draw(tubeTop3, tubeX3, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset3);
        batch.draw(tubeBottom3, tubeX3, (Gdx.graphics.getHeight() / 2) - tubeBottom3.getHeight() - tubeGap + tubeYOffset3);
        batch.draw(tubeTop4, tubeX4, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset4);
        batch.draw(tubeBottom4, tubeX4, (Gdx.graphics.getHeight() / 2) - tubeBottom4.getHeight() - tubeGap + tubeYOffset4);

        //Increments time and chooses flapping animation frame accordingly
        stateTime += Gdx.graphics.getDeltaTime();
        TextureRegion currentBirdFlap = animation.getKeyFrame(stateTime, true);

        //Draws bird frame according to ^^
        batch.draw(currentBirdFlap, (Gdx.graphics.getWidth() / 2) - (currentBirdFlap.getRegionWidth() / 2), birdY);

        //Draws current score
        font.draw(batch, Integer.toString(score), Gdx.graphics.getWidth() - 200, Gdx.graphics.getHeight() - 50);

        batch.end();

        //Creates circle / rectangles for collision detection
        birdCircle.set((Gdx.graphics.getWidth() / 2), (birdY + (currentBirdFlap.getRegionHeight() / 2)), (currentBirdFlap.getRegionHeight() / 2));
        tubeTopRect1.set(tubeX1, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset1, tubeTop1.getWidth(), tubeTop1.getHeight());
        tubeBottomRect1.set(tubeX1, (Gdx.graphics.getHeight() / 2) - tubeBottom1.getHeight() - tubeGap + tubeYOffset1, tubeBottom1.getWidth(), tubeBottom1.getHeight());
        tubeTopRect2.set(tubeX2, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset2, tubeTop2.getWidth(), tubeTop2.getHeight());
        tubeBottomRect2.set(tubeX2, (Gdx.graphics.getHeight() / 2) - tubeBottom2.getHeight() - tubeGap + tubeYOffset2, tubeBottom2.getWidth(), tubeBottom2.getHeight());
        tubeTopRect3.set(tubeX3, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset3, tubeTop3.getWidth(), tubeTop3.getHeight());
        tubeBottomRect3.set(tubeX3, (Gdx.graphics.getHeight() / 2) - tubeBottom3.getHeight() - tubeGap + tubeYOffset3, tubeBottom3.getWidth(), tubeBottom3.getHeight());
        tubeTopRect4.set(tubeX4, (Gdx.graphics.getHeight() / 2) + tubeGap + tubeYOffset4, tubeTop4.getWidth(), tubeTop4.getHeight());
        tubeBottomRect4.set(tubeX4, (Gdx.graphics.getHeight() / 2) - tubeBottom4.getHeight() - tubeGap + tubeYOffset4, tubeBottom4.getWidth(), tubeBottom4.getHeight());

        /* ShapeRenderer used for testing collision detection

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.CLEAR);

        shapeRenderer.circle(birdCircle.x, birdCircle.y, birdCircle.radius);
        shapeRenderer.rect(tubeTopRect1.x, tubeTopRect1.y, tubeTopRect1.width, tubeTopRect1.height);
        shapeRenderer.rect(tubeBottomRect1.x, tubeBottomRect1.y, tubeBottomRect1.width, tubeBottomRect1.height);
        shapeRenderer.rect(tubeTopRect2.x, tubeTopRect2.y, tubeTopRect2.width, tubeTopRect2.height);
        shapeRenderer.rect(tubeBottomRect2.x, tubeBottomRect2.y, tubeBottomRect2.width, tubeBottomRect2.height);
        shapeRenderer.rect(tubeTopRect3.x, tubeTopRect3.y, tubeTopRect3.width, tubeTopRect3.height);
        shapeRenderer.rect(tubeBottomRect3.x, tubeBottomRect3.y, tubeBottomRect3.width, tubeBottomRect3.height);
        shapeRenderer.rect(tubeTopRect4.x, tubeTopRect4.y, tubeTopRect4.width, tubeTopRect4.height);
        shapeRenderer.rect(tubeBottomRect4.x, tubeBottomRect4.y, tubeBottomRect4.width, tubeBottomRect4.height);

        shapeRenderer.end();  */

        //Checks for bird circle to touch any tube rectangle
        //Also triggers gameOver if bird falls off bottom of screen
        if (birdY < -currentBirdFlap.getRegionHeight() || Intersector.overlaps(birdCircle, tubeTopRect1) || Intersector.overlaps(birdCircle, tubeBottomRect1) || Intersector.overlaps(birdCircle, tubeTopRect2) || Intersector.overlaps(birdCircle, tubeBottomRect2) || Intersector.overlaps(birdCircle, tubeTopRect3) || Intersector.overlaps(birdCircle, tubeBottomRect3) || Intersector.overlaps(birdCircle, tubeTopRect4) || Intersector.overlaps(birdCircle, tubeBottomRect4)) {

            gameOver = true;

        }

        //Increments score when bird passes through tubes
        if (tubeX1 + (tubeTop1.getWidth() / 2) < (Gdx.graphics.getWidth() / 2) && readyToScore) {

            score++;
            readyToScore = false;

        }

        //Cascades tube coordinates backwards when tube #1 moves offscreen
        //Also sets readyToScore for next tube
        if ((tubeX1 + tubeTop1.getWidth()) < 0) {

            tubeX1 = tubeX1 + tubeDistance;
            tubeX2 = tubeX2 + tubeDistance;
            tubeX3 = tubeX3 + tubeDistance;
            tubeX4 = tubeX4 + tubeDistance;

            tubeYOffset1 = tubeYOffset2;
            tubeYOffset2 = tubeYOffset3;
            tubeYOffset3 = tubeYOffset4;
            tubeYOffset4 = (random.nextFloat() - 0.5f) * (Gdx.graphics.getHeight() - tubeGap - 500);

            readyToScore = true;

        }

        if (gameOver == true) {

            //Some copied code from main section to cause bird to fall off screen on gameOver
            birdVelocity = birdVelocity + gravity;
            birdY -= birdVelocity;
            stateTime += Gdx.graphics.getDeltaTime();
            TextureRegion gameOverBirdFlap = animation.getKeyFrame(stateTime, true);

            //Sets gameOver screen to show as half screen size
            //The current image works with x being 0, and getHeight / 4 is actually (getHeight / 2) - ((getHeight / 2) /2), aka center screen minus half the set image width
            //Also causes bird to fall off screen on gameOver
            batch.begin();
            batch.draw(currentBirdFlap, (Gdx.graphics.getWidth() / 2) - (gameOverBirdFlap.getRegionWidth() / 2), birdY);
            font.draw(batch, Integer.toString(score), Gdx.graphics.getWidth() - 200, Gdx.graphics.getHeight() - 50);
            batch.draw(gameover, 0, (Gdx.graphics.getHeight() / 4), Gdx.graphics.getWidth(), (Gdx.graphics.getHeight() / 2));

            batch.end();

            //Restarts game when gameOver screen clicked
            if (Gdx.input.justTouched()) {

                dispose();
                create();
            }
        }
	}

	
	@Override
	public void dispose () {

		batch.dispose();
		background.dispose();
        tubeTop1.dispose();
        tubeBottom1.dispose();
        tubeTop2.dispose();
        tubeBottom2.dispose();
        tubeTop3.dispose();
        tubeBottom3.dispose();
        tubeTop4.dispose();
        tubeBottom4.dispose();
        gameover.dispose();

	}
}
